﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurant
{
    class Cashier:Personne
    {
        public Cashier(string s) : base(s)
        {

        }
        public void Pay()
        {
            Console.WriteLine("{0}: Voici l'addition.", this.Nom);
        }
    }
}
