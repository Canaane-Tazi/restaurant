﻿using System;

namespace Restaurant
{
    class Program
    {
        static void Main(string[] args)
        {
            Cook Chef = new Cook("Chef");
            Client client = new Client("Fab");
            Cashier caissier = new Cashier("Mohamed");
            Waiter serveur = new Waiter("Léo", Chef, client, caissier);
            client.sitAtATable(serveur);
            Console.ReadLine();
        }
    }
}
