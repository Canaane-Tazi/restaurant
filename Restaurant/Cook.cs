﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurant
{
    class Cook:Personne
    {
        public Cook(string s) : base(s)
        {

        }
        public void OrderFood(Waiter w)
        {
            Console.WriteLine("{0}: Voici votre plat",w.Nom);
            Console.WriteLine("{0}: C'est prêt !",this.Nom);
            w.PickUp();
        }
    }
}
