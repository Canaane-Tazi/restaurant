﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurant
{
    class Personne
    {
        protected string nom;

        public Personne(string s)
        {
            this.nom = s;
        }
        public string Nom { get { return this.nom; } set{ this.nom = value; } }
    }
}
