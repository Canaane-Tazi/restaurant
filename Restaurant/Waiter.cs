﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurant
{
    class Waiter:Personne
    {
        protected Cook chef;
        protected Client monClient;
        protected Cashier caissier;

        public Waiter(string s, Cook Chef, Client Client, Cashier Caissier) : base(s)
        {
            this.chef = Chef;
            this.monClient = Client;
            this.caissier = Caissier;
        }
        public void OrderFood(Client c)
        {
            c.ServeWine();
            Console.WriteLine("{0}: On a une commande Chef.", this.Nom);
            this.chef.OrderFood(this);
            this.monClient = c;
        }
        public void PickUp()
        {
            Console.WriteLine("{0}: Bien vu Chef.",this.Nom);
            this.monClient.ServeFood(this.caissier);
        }
    }
}
