﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurant
{
    class Client : Personne
    {
        public Client(string s) : base(s)
        {

        }
        public void sitAtATable(Waiter w)
        {
            Console.WriteLine("{0}: Serveur ?", this.Nom);
            w.OrderFood(this);
        }
        public void ServeWine()
        {
            Console.WriteLine("{0}: Quel vin délicieux !", this.Nom);
        }
        public void ServeFood(Cashier c)
        {
            Console.WriteLine("{0}: Je voudrais payer svp.", this.Nom);
            c.Pay();
        }
    }
}
